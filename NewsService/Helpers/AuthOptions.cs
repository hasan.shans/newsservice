﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsService.Helpers
{
    public class AuthOptions
    {
        public const string ISSUER = "NewsService"; // издатель токена
        public const string AUDIENCE = "NewsServiceAdmin"; // потребитель токена
        const string KEY = "ibomuahcha3x";   // ключ для шифрации
        public const int LIFETIME = 1; // время жизни токена - 1 минута
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
