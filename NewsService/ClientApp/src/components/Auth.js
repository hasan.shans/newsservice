import React, { Component } from 'react';
import decode from "jwt-decode";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    NavLink,
    Redirect
  } from "react-router-dom";

const checkAuth = () => {
    const token = localStorage.getItem('token');
    const refreshToken = localStorage.getItem('refreshToken');
    if(!token || !refreshToken){
      return false;
    } 

    try{
        const {exp} = decode(refreshToken);

        if(exp < new Date().getTime()){
            return false;
        }
    }
    catch(e){
        return false;
    }
  
    return true;
  }

  export const AuthRoute = ({ component: Component, ...rest })  => {
    return (
      <Route {...rest} render={props =>
          checkAuth() ? 
          (
            <Component {...props}/>
          ) : (
            <Redirect to={{ pathname: "/login" }}/>
          )
        }
      />
    );
  }
  

// const Auth = () => {
//     render() {
//         return (
//             <div>
                
//             </div>
//         );
//     }
// }

