import React, { useState } from 'react';
import axios from "axios";

const Login = () => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const onLogin = async (e) => {
        e.preventDefault();
        const response = await axios.post('/token', { username, password });
        console.log(response);  
    }

    return (
        <div>
            <div className="container mt-1 d-flex justify-content-center">
                <div className="row">
                    <div className="col-md-12">
                        <form method="POST" onSubmit={onLogin}>
                            <input class="form-control" name="username" placeholder="Login" onChange={e => setUsername(e.target.value)}/>
                            <input class="form-control" name="password" placeholder="password" type="password" onChange={e => setPassword(e.target.value)} />
                            <input class="form-control" type="submit" text="Login"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;