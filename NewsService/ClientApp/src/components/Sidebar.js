import React, { Component} from 'react';


const Sidebar = () => {
    return (
        <div class="col-md-4">
            <div class="aside-widget text-center">
                <a href="#" style={{display: "inline-block;margin: auto;"}}>
                    <img class="img-responsive" src="./img/ad-1.jpg" alt=""/>
                </a>
            </div>
            <div class="aside-widget">
                <div class="section-title">
                    <h2>Most Read</h2>
                </div>

                <div class="post post-widget">
                    <a class="post-img" href="blog-post.html"><img src="./img/widget-1.jpg" alt=""/></a>
                    <div class="post-body">
                        <h3 class="post-title"><a href="blog-post.html">Tell-A-Tool: Guide To Web Design And Development Tools</a></h3>
                    </div>
                </div>

                <div class="post post-widget">
                    <a class="post-img" href="blog-post.html"><img src="./img/widget-2.jpg" alt=""/></a>
                    <div class="post-body">
                        <h3 class="post-title"><a href="blog-post.html">Pagedraw UI Builder Turns Your Website Design Mockup Into Code Automatically</a></h3>
                    </div>
                </div>

                <div class="post post-widget">
                    <a class="post-img" href="blog-post.html"><img src="./img/widget-3.jpg" alt=""/></a>
                    <div class="post-body">
                        <h3 class="post-title"><a href="blog-post.html">Why Node.js Is The Coolest Kid On The Backend Development Block!</a></h3>
                    </div>
                </div>

                <div class="post post-widget">
                    <a class="post-img" href="blog-post.html"><img src="./img/widget-4.jpg" alt=""/></a>
                    <div class="post-body">
                        <h3 class="post-title"><a href="blog-post.html">Tell-A-Tool: Guide To Web Design And Development Tools</a></h3>
                    </div>
                </div>
            </div>
            <div class="aside-widget">
                <div class="section-title">
                    <h2>Catagories</h2>
                </div>
                <div class="category-widget">
                    <ul>
                        <li><a href="#" class="cat-1">Web Design<span>340</span></a></li>
                        <li><a href="#" class="cat-2">JavaScript<span>74</span></a></li>
                        <li><a href="#" class="cat-4">JQuery<span>41</span></a></li>
                        <li><a href="#" class="cat-3">CSS<span>35</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
						
    );
}

export default Sidebar;