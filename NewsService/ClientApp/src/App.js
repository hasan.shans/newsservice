import 'bootstrap/dist/css/bootstrap.min.css';
import './css/style.css';

import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink
} from "react-router-dom";

import { Header, Footer } from './components/Layout';
import Home from "./components/Home";
import Sidebar from './components/Sidebar';
import  Login  from './components/Login';
import { AuthRoute } from './components/Auth';
import Post from './components/Post';

export default class App extends Component {
  static displayName = App.name;
  render () {
    return (
      <React.Fragment>
        <Header/>

          <Switch>
            <Route exact path="/" component={Home}/> 
            <Route exact path="/login" component={Login}/> 
            <AuthRoute exact path="/page" component={Post}/> 
          </Switch>

          
          
        <Footer/>
        </React.Fragment>
    );
  }
}
