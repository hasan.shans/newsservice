﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsService.Models
{
    public class PersonLoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
