﻿using System;
using System.Collections.Generic;

namespace NewsService.Models
{
    public partial class Posts
    {
        public int Id { get; set; }
        public string NewsTitle { get; set; }
        public string NewsContent { get; set; }
        public DateTime PostedDate { get; set; }
        public int Views { get; set; }
        public int UserId { get; set; }

        public virtual Users User { get; set; }
    }
}
